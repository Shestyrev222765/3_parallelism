from multiprocessing import Pool, cpu_count
import numpy as np
import pandas as pd
import sys


# Функция перемножения элементов матриц
def element(index, A, B):
    i, j = index
    res = 0
    N = len(A[0]) or len(B)
    for k in range(N):
        res += A[i][k] * B[k][j]
    print(res)
    f = open("res_test.csv", "+a")
    if j + 1 == k:
        f.write(f"{res:.2f}\n")
    else:
        f.write(f"{res:.2f}, ")
    f.close()
    return res

# Функция для распараллеливания вычислений
def parallel_multiply_matrices(A, B):
    if len(A[0]) != len(B):
        raise ValueError("Количество столбцов матрицы A должно быть равно количеству строк матрицы B")

    # Создаем список индексов для каждого элемента результирующей матрицы
    indices = [(i, j) for i in range(len(A)) for j in range(len(B[0]))]

    # Создаем пул процессов, используя количество доступных ядер процессора
    with Pool(cpu_count()) as pool:
        # Выполняем перемножение элементов матриц параллельно с помощью функции element
        f = open("res_test.csv", mode="w")
        f.close()
        result_elements = pool.starmap(element, [(index, A, B) for index in indices])

    # Преобразуем список результатов в матрицу
    result_matrix = np.reshape(result_elements, (len(A), len(B[0])))

    return result_matrix

def read_from_file(path_to_a="A.csv", path_to_b="B.csv"):
    A = pd.read_csv(path_to_a, header=None).to_numpy()
    B = pd.read_csv(path_to_b, header=None).to_numpy()
    return A, B

if __name__ == '__main__':
    choose = int(input("How fill Matrix\n1: Random\n2: From file\n"))
    if choose == 1:
        a, b = map(int, input("Input size AxB for Matrix A, for example: 5, 5 ").split(','))
        c, d = map(int, input("Input size AxB for Matrix B, for example: 5, 5 ").split(','))
        # Создаем две случайные матрицы размером 5x5
        A = np.random.randint(0, 10, size=(a, b))
        B  = np.random.randint(0, 10, size=(c, d))
    elif choose == 2:
        print("Please input path to file for Matrix A and Matrix B default A.csv and Bcsv")
        path_a = input("A path: ")
        path_b = input("B path: ")
        path_a = path_a if path_a else "A.csv"
        path_b = path_b if path_b else "B.csv"
        A, B = read_from_file(path_a, path_b)
    else:
        print("Incorrect choose!")
        sys.exit(1)

    try:
        # Выполняем перемножение матриц с использованием многопроцессорности
        result = parallel_multiply_matrices(A, B)

        # Выводим результат
        print("Matrix A:")
        print(A)
        print("\nMatrix B:")
        print(B)
        print("\nResult:")
        print(result)
        res_numpy = np.array(result)
        np.savetxt("res.csv", res_numpy, delimiter=",", fmt="%.2f")
    except ValueError as e:
        print(e)
